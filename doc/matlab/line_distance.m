function [ dist ] = line_distance(x, y, a, b)
dist = abs((a*x + b*y)/(sqrt(a*a + b*b)));
end

