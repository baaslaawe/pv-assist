#! /bin/bash

sh ./pocketsphinx_continuous \
-fwdflat no \
-bestpath no \
-lm /usr/share/pocketsphinx/model/lm/voxforge/voxforge_en_sphinx.lm \
-dict /usr/share/pocketsphinx/model/lm/voxforge/cmudict.0.7a \
-hmm /usr/share/pocketsphinx/model/hmm/voxforge \
-samprate 8000 \
-nfft 256 \
-infile /home/kvant/asterisk-voice-dump.wav 
