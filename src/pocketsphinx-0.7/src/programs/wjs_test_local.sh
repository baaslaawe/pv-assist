#! /bin/bash

sh ./pocketsphinx_continuous \
-fwdflat no \
-bestpath no \
-samprate 8000 \
-lm /usr/share/pocketsphinx/model/lm/test_nsh/pvmanager-srt.lm.dmp \
-dict /usr/share/pocketsphinx/model/lm/test_nsh/cmu07a.dic \
-hmm /usr/share/pocketsphinx/model/hmm/test_nsh/ \
-nfft 256 \
-backtrace yes \
-infile /home/kvant/asterisk-voice-dump.wav

