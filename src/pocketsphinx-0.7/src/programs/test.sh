#! /bin/bash

sh ./pocketsphinx_continuous \
-fwdflat no \
-bestpath no \
-samprate 8000 \
-lm /usr/share/pocketsphinx/model/lm/web_test_simple/4505.lm \
-dict /usr/share/pocketsphinx/model/lm/web_test_simple/4505.dic \
-hmm /usr/share/pocketsphinx/model/hmm/communicator \
-subvq      /usr/share/pocketsphinx/model/hmm/communicator/subvq \
-beam       1e-60 \
-wbeam      1e-40 \
-ci_pbeam   1e-8 \
-subvqbeam  1e-2 \
-maxhmmpf   2000 \
-maxcdsenpf 1000 \
-maxwpf     8 \
-ds         2 \
-nfft 256 \
-backtrace yes \
-infile /home/kvant/asterisk-voice-dump.wav 
