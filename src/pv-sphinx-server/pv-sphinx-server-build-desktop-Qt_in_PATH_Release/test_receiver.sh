#!/bin/bash

let port=8080
user="testserver"
for i in {4..500}
do
   let port=$port+1
   curuser="$user""$i"

   #PRODPID[$i]=$!
   ./pv-sphinx-server -fwdflat no -samprate 8000 -lm /home/kvant/src/mitlm.0.4/test1.lm -dict /usr/share/pocketsphinx/model/lm/test_nsh/cmu07a.dic -hmm /usr/share/pocketsphinx/model/hmm/test_nsh/ -nfft 256 -backtrace yes   -bestpath yes -inbuffer no -infile /home/kvant/asterisk-voice-dump.wav -domain 192.168.0.56 -pwd 685702 -user $curuser -port $port &
   PRODPID[$i]=$!
   echo "Process id = $PRODPID[$i]"
done

export PRODPID


# record own PID
export PID=$$

# define exit function
exit_timeout() {
  echo "Timeout. These processes are not finished:"
  for i in ${PRODPID[@]} ; do
    ps -p $i |grep -v "PID TTY      TIME CMD"
    if [ $? == 0 ] ; then
      # process still alive
      echo "Sending SIGTERM to process $i"
      kill $i
    fi
  done
  # timeout exit
  exit
}

# Handler for signal USR1 for the timer
trap exit_timeout SIGUSR1
trap exit_timeout INT

# starting timer in subshell. It sends a SIGUSR1 to the father if it timeouts.
export TIMEOUT=600
(sleep $TIMEOUT ; kill -SIGUSR1 $PID) &

# record PID of timer
TPID=$!


# wait for all production processes to finish
wait ${PRODPID[*]}

# Normal exit
echo "All processes ended normal"

# kill timer
kill $TPID
