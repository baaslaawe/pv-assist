#!/bin/bash

let port=5080
user="testuser"
for i in {4..50}
do
   let port=$port+1
   curuser="$user""$i"
   ./pv-sphinx-server -fwdflat no -samprate 8000 -lm /home/kvant/src/mitlm.0.4/test1.lm -dict /usr/share/pocketsphinx/model/lm/test_nsh/cmu07a.dic -hmm /usr/share/pocketsphinx/model/hmm/test_nsh/ -nfft 256 -backtrace yes   -bestpath yes -inbuffer no -infile /home/kvant/asterisk-voice-dump.wav -domain 192.168.0.56 -user $curuser -pwd 685702 -calluri sip:1243$i@192.168.0.56  -sndvoicedump ../test_data/send_message.wav -port $port &
   PRODPID[$i]=$!
   echo "Process id = $PRODPID[$i]"
   sleep 45
done


export PRODPID


# record own PID
export PID=$$

# define exit function
exit_timeout() {
  echo "Timeout. These processes are not finished:"
  for i in ${PRODPID[@]} ; do
    ps -p $i |grep -v "PID TTY      TIME CMD"
    if [ $? == 0 ] ; then
      # process still alive
      echo "Sending SIGTERM to process $i"
      kill $i
    fi
  done
  # timeout exit
  exit
}

# Handler for signal USR1 for the timer
trap exit_timeout SIGUSR1
trap exit_timeout INT

# starting timer in subshell. It sends a SIGUSR1 to the father if it timeouts.
export TIMEOUT=600
(sleep $TIMEOUT ; kill -SIGUSR1 $PID) &

# record PID of timer
TPID=$!


# wait for all production processes to finish
wait ${PRODPID[*]}

# Normal exit
echo "All processes ended normal"

# kill timer
kill $TPID
