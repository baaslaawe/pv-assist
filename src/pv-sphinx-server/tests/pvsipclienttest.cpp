#include "pvsipclienttest.h"
#include "pvsipclient.h"
#include "pvringbuffer.h"
#include <memory>
#include <sys/types.h>
#include <sys/stat.h>

CPPUNIT_TEST_SUITE_REGISTRATION( PVSipClientTest );

#define VOICE_BUFFER_PORTION_SIZE 128
#define MAX_WAITINGTIME 60
#define SAMPLE_RATE 8000

void PVSipClientTest::tearDown()
{
    struct stat outfilestatus;
    stat("../test_data/sip_client_output8000.wav", &outfilestatus );
    CPPUNIT_ASSERT(outfilestatus.st_size != 0);
}

void PVSipClientTest::setUp()
{
}

void PVSipClientTest::ConnectAndLogData()
{
    std::auto_ptr<PVRingBuffer> sharedBuffer(new PVRingBuffer(MAX_WAITINGTIME * SAMPLE_RATE * 2));
    std::auto_ptr<PVSipClient> lpClient(new PVSipClient(VOICE_BUFFER_PORTION_SIZE, sharedBuffer.get(),
                                                        NULL, NULL));
    lpClient->Connect();
    sleep(40);
}
