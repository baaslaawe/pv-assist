#ifndef PVRATECONVERTERTEST_H
#define PVRATECONVERTERTEST_H

#include <cppunit/extensions/HelperMacros.h>

class PVRateConverterTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( PVRateConverterTest );
    //CPPUNIT_TEST( RateConvert );
    CPPUNIT_TEST( RateConvert2 );
    CPPUNIT_TEST_SUITE_END();
public:
    void virtual tearDown();
    void virtual setUp();
protected:
    FILE * m_file;
    FILE * m_newfile;
    void RateConvert();
    void RateConvert2();
};

#endif // PVRATECONVERTERTEST_H
