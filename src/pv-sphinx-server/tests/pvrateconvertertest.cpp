#include "pvrateconvertertest.h"
#include "../pvrateconverter.h"
#include <cppunit/config/SourcePrefix.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <memory>
#include <vector>

CPPUNIT_TEST_SUITE_REGISTRATION( PVRateConverterTest );

void PVRateConverterTest::tearDown()
{
    fclose(m_file);
    fclose(m_newfile);

    struct stat outfilestatus;
    stat("../test_data/synth_voice8000.wav", &outfilestatus );
    CPPUNIT_ASSERT(outfilestatus.st_size != 0);
}

void PVRateConverterTest::setUp()
{
    m_file = fopen("../test_data/synth_voice16000.wav", "r");
    m_newfile = fopen("../test_data/synth_voice8000.wav", "w");
}

void PVRateConverterTest::RateConvert()
{
    if (m_file == NULL || m_newfile == NULL)
        CPPUNIT_FAIL("worng file descriptor");

    struct stat filestatus;
    std::vector<short> input, output;
    stat("../test_data/synth_voice16000.wav", &filestatus );

    input.resize(filestatus.st_size / 2);

    int bytes_read = fread(&input[0], 2, filestatus.st_size, m_file);

    if (bytes_read != filestatus.st_size / 2)
        CPPUNIT_FAIL("wrong file size");

    std::auto_ptr<PVRateConverter> rateConv(new PVRateConverter(16000, 8000));
    CPPUNIT_ASSERT(rateConv->Resample(input, output));

    if (output.size() != 0)
        fwrite (&output[0], 2, output.size(), m_newfile);
}

void PVRateConverterTest::RateConvert2()
{
    if (m_file == NULL || m_newfile == NULL)
        CPPUNIT_FAIL("worng file descriptor");

    struct stat filestatus;
    std::vector<char> input, output;
    stat("../test_data/synth_voice16000.wav", &filestatus );

    input.resize(filestatus.st_size);

    int bytes_read = fread(&input[0], 1, filestatus.st_size, m_file);

    if (bytes_read != filestatus.st_size)
        CPPUNIT_FAIL("wrong file size");

    std::auto_ptr<PVRateConverter> rateConv(new PVRateConverter(16000, 8000));
    CPPUNIT_ASSERT(rateConv->Resample(input, output));

    if (output.size() != 0)
        fwrite (&output[0], 1, output.size(), m_newfile);
}
