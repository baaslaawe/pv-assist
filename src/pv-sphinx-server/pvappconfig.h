#ifndef PVAPPCONFIG_H
#define PVAPPCONFIG_H

#include <map>
#include <string>
#include <mysql/mysql.h>

#define TEST_RUNNING_ARG "runtests"

/*Sip command line arguments*/
#define SIP_PLACE_CALL_ARG "calluri"
#define SIP_DOMAIN_ARG "domain"
#define SIP_USER_ARG "user"
#define SIP_PASSWORD_ARG "pwd"
#define SIP_PORT "port"

/*IO operation line arguments*/
#define IO_RECEIVE_VOICE_DUMP "rcvvoicedump"
#define IO_SEND_VOICE_DUMP "sndvoicedump"
#define IO_PLAY_SOUND_FILE "playfile"


#define DB_RESULTS_TABLE "sr_log"
#define DB_USERFROM_FIELD "UserFrom"
#define DB_USERTO_FIELD "UserTo"
#define DB_PHRASE_FIELD "Phrase"
#define DB_TIMESTAMP_FIELD "Time"


/*mysql credentials*/
#define USER "root"
#define PASSWORD "685702"
#define INITDB "pv-assist"

class PVAppConfig
{
private:
    int m_argc;
    char ** m_argv;
    MYSQL m_mySql;
    MYSQL * m_mySqlConnection;

    typedef std::map<std::string, std::string> MAPSTR2STR;
    MAPSTR2STR m_aParams;
    bool CreateMySqlConnection();

public:
    PVAppConfig();
    ~PVAppConfig();
    bool ParseCmdLineArgs(int argc, char *argv[]);
    inline bool IsRunTests() {return (HasParam(TEST_RUNNING_ARG));}
    int GetArgCount() {return (m_argc);}
    char ** GetArgValues() {return (m_argv);}
    MYSQL * GetMySqlConnection();

    std::string GetParamValue(std::string name);
    bool HasParam(std::string name);
    int GetParamsAmount() {return (m_aParams.size());}
};

#endif // PVAPPCONFIG_H
