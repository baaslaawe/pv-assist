#ifndef PVRINGBUFFER_H
#define PVRINGBUFFER_H

#include <vector>
#include "ringbuffer.h"

/*Wrapper class over lock-free ring buffer*/
class PVRingBuffer
{
private:
    jack_ringbuffer_t * m_buffer;
public:
    PVRingBuffer(int iSize);
    ~PVRingBuffer();
    bool WriteData(char * data, int iSize);
    int ReadData(char *data, int iSize);
};

#endif // PVRINGBUFFER_H
