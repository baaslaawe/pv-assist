#ifndef PVSPEECHRECOGNIZEDMESSAGE_H
#define PVSPEECHRECOGNIZEDMESSAGE_H

#include "pvtext2speechmessage.h"
#include <vector>
#include <string>
#include "../pocketsphinx-0.7/include/pocketsphinx.h"

class PVSpeechRecognizedMessage : public PVText2SpeechMessage
{
public:
    struct NBestCandidate
    {
        std::vector<std::string> wordSequence;
        std::vector<int> wordScore;
        std::vector<int> wordSequenceId;
        int iScore;
        std::string sText;
    };
    typedef std::vector<NBestCandidate> CANDIDATELIST;
private:
    int m_iBestScore;
   CANDIDATELIST m_CandidateList;
public:
    PVSpeechRecognizedMessage(std::string sText);
    virtual ~PVSpeechRecognizedMessage();
    bool ImportFromPocketSphinx(ps_nbest_ext_t * extList, int extListLength);
    CANDIDATELIST * GetCandidateList() {return (&m_CandidateList);}
};

#endif // PVSPEECHRECOGNIZEDMESSAGE_H
