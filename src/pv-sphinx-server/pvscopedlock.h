#ifndef PVSCOPEDLOCK_H
#define PVSCOPEDLOCK_H

#include "pthread.h"

class PVScopedLock
{
private:
    pthread_mutex_t * m_lpMutex;
public:
    PVScopedLock(pthread_mutex_t * lpMutex);
    void unlock();
    ~PVScopedLock();
};

#endif // PVSCOPEDLOCK_H
