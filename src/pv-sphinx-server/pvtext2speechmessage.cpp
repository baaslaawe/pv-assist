#include "pvtext2speechmessage.h"

PVText2SpeechMessage::PVText2SpeechMessage(std::string sText)
{
    m_typeId = PVMessage::TEXT_TO_SPEECH_MSG;
    m_sRecognitionResult = sText;
}
