#ifndef PVDBSAVECOMMAND_H
#define PVDBSAVECOMMAND_H

#include "./pvbasecommand.h"
#include <tr1/memory>

class PVMessageObserver;
class PVDBMessage;

class PVDbSaveCommand : public PVBaseCommand
{
private:
    PVDBMessage * m_lpMsg;
    int Save2MySql(std::string names, std::string values);
public:
    PVDbSaveCommand(PVMessageObserver * lpReceiver, PVMessage * lpMsg);
    virtual ~PVDbSaveCommand();
    virtual bool Execute();
};

#endif // PVDBSAVECOMMAND_H
