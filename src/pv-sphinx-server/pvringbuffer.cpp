#include "pvringbuffer.h"

PVRingBuffer::PVRingBuffer(int iSize)
{
    m_buffer = jack_ringbuffer_create(iSize);
}

PVRingBuffer::~PVRingBuffer()
{
    jack_ringbuffer_free(m_buffer);
}

bool PVRingBuffer::WriteData(char * data, int iSize)
{
    if (iSize == 0)
        return (false);

    size_t writtenBytes = jack_ringbuffer_write(m_buffer, data, iSize);
    if (writtenBytes < iSize)
        return (false);
    else
        return (true);
}

int PVRingBuffer::ReadData(char * data, int iSize)
{
    if (iSize == 0)
        return(-1);

    return (jack_ringbuffer_read(m_buffer, data, iSize));
}
