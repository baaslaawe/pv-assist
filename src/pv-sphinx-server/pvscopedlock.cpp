#include "pvscopedlock.h"

PVScopedLock::PVScopedLock(pthread_mutex_t * lpMutex)
{
    m_lpMutex = lpMutex;
    if (m_lpMutex != NULL)
        pthread_mutex_lock(m_lpMutex);
}

void PVScopedLock::unlock()
{
    if (m_lpMutex != NULL)
        pthread_mutex_unlock(m_lpMutex);
    m_lpMutex = NULL;
}

PVScopedLock::~PVScopedLock()
{
    if (m_lpMutex != NULL)
        pthread_mutex_unlock(m_lpMutex);
}
