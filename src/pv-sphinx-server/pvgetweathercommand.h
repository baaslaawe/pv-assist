#ifndef PVGETWEATHERCOMMAND_H
#define PVGETWEATHERCOMMAND_H
#include "pvbasecommand.h"

/*Class of command for getting current weather from open web API with next url:
http://free.worldweatheronline.com/feed/weather.ashx?q=Moscow&format=csv&num_of_days=2&key=043d986045101646120105
*/
class PVGetWeatherCommand : public PVBaseCommand
{
private:
    /*Structure for storing weather query results according to:
    #date,tempMaxC,tempMaxF,tempMinC,tempMinF,windspeedMiles,
    windspeedKmph,winddirDegree,winddir16Point,weatherCode,
    weatherIconUrl,weatherDesc,precipMM
    */
    struct WeatherData
    {
        std::string date;
        std::string tempMaxC;
        std::string tempMinC;
        std::string windspeedKmph;
    };
    void ParseWeatherServerResponse(std::string response, WeatherData & weather);
    std::string CreateWeatherAnswer(const WeatherData & weather);
public:
    PVGetWeatherCommand(PVMessageObserver *lpReceiver);
    virtual ~PVGetWeatherCommand();
    virtual bool Execute();
};

#endif // PVGETWEATHERCOMMAND_H
