#ifndef PVBASECOMMAND_H
#define PVBASECOMMAND_H
#include <string>

class PVMessageObserver;
class PVMessage;

/*Base class for commands which system should maintain in response to user request.*/
class PVBaseCommand
{
protected:
    PVMessageObserver * m_lpReceiver;
    std::string m_sText;
public:
    static PVBaseCommand * CreateCommand(PVMessage * lpMsg, PVMessageObserver *lpReceiver);
    PVBaseCommand(PVMessageObserver * lpReceiver, std::string sText);
    virtual ~PVBaseCommand();
    virtual bool Execute();
};

#endif // PVBASECOMMAND_H
