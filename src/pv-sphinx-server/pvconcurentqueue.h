#ifndef PVCONCURENTQUEUE_H
#define PVCONCURENTQUEUE_H

#include <queue>
#include "pvscopedlock.h"
#include <cerrno>

template<typename T>
class PVConcurentQueue
{  
private:
    mutable pthread_mutex_t m_mutex;
    std::queue<T> m_dataQueue;
    pthread_cond_t m_dataCond;
    mutable bool m_bDestroyCondition;
public:
    PVConcurentQueue()
        :m_bDestroyCondition(false)
    {
    }
    PVConcurentQueue(PVConcurentQueue const& other)
    {
        PVScopedLock lock(&other.mut);
        m_dataQueue = other.m_dataQueue;
    }
    void set_destroy_condition()
    {
        PVScopedLock guard(&m_mutex);
        m_bDestroyCondition = true;
        guard.unlock();

        pthread_cond_signal(&m_dataCond);
    }

    void push(T data)
    {
        PVScopedLock guard(&m_mutex);
        m_dataQueue.push(data);
        guard.unlock();

        pthread_cond_signal(&m_dataCond);
    }
    void wait_and_pop(T& value)
    {
        PVScopedLock guard(&m_mutex);
        while(m_dataQueue.empty())
            pthread_cond_wait(&m_dataCond, &m_mutex);

        value = m_dataQueue.front();
        m_dataQueue.pop();
    }
    bool wait_and_pop_safe(T& value)
    {
        PVScopedLock guard(&m_mutex);
        pthread_cond_wait(&m_dataCond, &m_mutex);

        if (!m_dataQueue.empty())
        {
            value = m_dataQueue.front();
            m_dataQueue.pop();
            return true;
        }
        else
        {
            if (m_bDestroyCondition)
                return (false);
            else
                return (true);
        }

    }
    T wait_and_pop()
    {
        PVScopedLock lock(&m_mutex);
        while(m_dataQueue.empty())
            pthread_cond_wait(&m_dataCond, &m_mutex);

        T res = m_dataQueue.front();
        m_dataQueue.pop();
        return res;
    }
    T wait_and_pop(int iWaitTimeout)
    {
        timespec WaitTime;
        WaitTime.tv_nsec = iWaitTimeout;
        WaitTime.tv_sec = 0;

        PVScopedLock lock(&m_mutex);
        while(m_dataQueue.empty())
            if (pthread_cond_timedwait(&m_dataCond, &m_mutex, &WaitTime) == ETIMEDOUT)
                return T();

        T res = m_dataQueue.front();
        m_dataQueue.pop();
        return res;
    }
    bool try_pop(T& value)
    {
        PVScopedLock lock(&m_mutex);
        if(m_dataQueue.empty())
            return false;

        value = m_dataQueue.front();
        m_dataQueue.pop();
    }
    T try_pop()
    {
        PVScopedLock lock(&m_mutex);
        if(m_dataQueue.empty())
            return T();

        T res = m_dataQueue.front();
        m_dataQueue.pop();
        return res;
    }
    bool empty() const
    {
        PVScopedLock lock(&m_mutex);
        return m_dataQueue.empty();
    }
};



#endif // PVCONCURENTQUEUE_H
