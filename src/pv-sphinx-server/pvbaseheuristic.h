#ifndef PVBASEHEURISTIC_H
#define PVBASEHEURISTIC_H

class PVSpeechRecognizedMessage;
struct InternalLM;

/*Base abstract class that represents heuristics for analisys of extended N-best search results contained in
PVSpeechRecognizedMessage.*/
class PVBaseHeuristic
{
protected:
    InternalLM *m_lpLM;
    float m_iProbability;
public:
    PVBaseHeuristic(InternalLM *lpLM, float iProbability);
    float GetProbabiliry() {return (m_iProbability);}
    virtual bool Execute(PVSpeechRecognizedMessage * lpMsg) = 0;
};

class PVFindLowCandidateHeuristic : public PVBaseHeuristic
{
public:
    PVFindLowCandidateHeuristic(InternalLM *lpLM);
    virtual bool Execute(PVSpeechRecognizedMessage *lpMsg);
};

class PVNotEqualHeuristic : public PVBaseHeuristic
{
public:
    PVNotEqualHeuristic(InternalLM *lpLM);
    virtual bool Execute(PVSpeechRecognizedMessage *lpMsg);
};

class PVNotInSourceLMHeuristic : public PVBaseHeuristic
{
public:
    PVNotInSourceLMHeuristic(InternalLM *lpLM);
    virtual bool Execute(PVSpeechRecognizedMessage *lpMsg);
};


#endif // PVBASEHEURISTIC_H
