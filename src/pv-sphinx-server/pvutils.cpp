#include "pvutils.h"

#include <algorithm>
#include <functional>
#include <locale>
#include <cctype>

namespace pvutils
{
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    return split(s, delim, elems);
}

// trim from both ends
std::string &trim(std::string &s)
{
    return ltrim(rtrim(s));
}

// trim from start
std::string &ltrim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
std::string &rtrim(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

bool isdigit(std::string &s)
{
    //TODO - refactor here
    if (std::find_if(s.begin(), s.end(), (int(*)(int))std::isalpha) != s.end())
    {
        // contains something else
        return (false);
    }
    else
        return (true);
}



}
