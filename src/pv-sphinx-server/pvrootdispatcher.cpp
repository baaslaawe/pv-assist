#include "pvrootdispatcher.h"
#include "pvsipclient.h"
#include "pvringbuffer.h"
#include "pvspeechrecognizer.h"
#include "pvspeechsynthesizer.h"
#include "pvbasecommand.h"
#include "pvmessage.h"
#include "pvheuristicanalyzer.h"
#include "pvappconfig.h"

#define VOICE_BUFFER_PORTION_SIZE 128
#define MAX_WAITINGTIME 10
#define SAMPLE_RATE 8000

extern PVAppConfig g_appConfig;

void PVRootDispatcher::OnMessage(std::tr1::shared_ptr<PVMessage> inMsg)
{
    switch(inMsg->GetType())
    {
    case PVMessage::SIP_CONNECTED_MSG:
        {
            //TODO - think about this recreation logic
            m_Recognizer.reset(new PVSpeechRecognizer(g_appConfig.GetArgCount(), g_appConfig.GetArgValues(),
                                                     VOICE_BUFFER_PORTION_SIZE, m_sharedInputBuffer.get(), this));
            m_Synthesizer.reset(new PVSpeechSynthesizer(m_sharedOutputBuffer.get()));
            m_Analyser.reset(new PVHeuristicAnalyzer(this, m_Recognizer->GetSourceLM()));
            break;
        }
    case PVMessage::SIP_DISCONNECTED_MSG:
        {
            m_Recognizer.reset(NULL);
            m_Synthesizer.reset(NULL);
            break;
        }
    case PVMessage::SIP_MEDIA_STARTED_MSG:
        {
            if (m_Recognizer.get() != NULL)
                m_Recognizer->Start();
            if (m_Synthesizer.get() != NULL)
                m_Synthesizer->Start();
            break;
        }
    case PVMessage::TEXT_TO_SPEECH_MSG:
        {
            if (m_Synthesizer.get() != NULL)
            {
                std::auto_ptr<PVBaseCommand> cmd(PVBaseCommand::CreateCommand(inMsg.get(), m_Synthesizer.get()));
                cmd->Execute();
            }
            break;
        }
    case PVMessage::RECOGNITION_RESULT_MSG:
        {
            if (m_Analyser.get() != NULL)
                m_Analyser->SendMessageSync(inMsg);
            break;
        }
    case PVMessage::DB_TASK_MSG:
        {
            std::auto_ptr<PVBaseCommand> cmd(PVBaseCommand::CreateCommand(inMsg.get(), NULL));
            cmd->Execute();
            break;
        }
    default:
        {
            break;
        }
    }
}

PVRootDispatcher::PVRootDispatcher()
    :m_sharedInputBuffer(new PVRingBuffer(MAX_WAITINGTIME * SAMPLE_RATE * 2)),
     m_sharedOutputBuffer(new PVRingBuffer(MAX_WAITINGTIME * SAMPLE_RATE * 4))
{
}

PVRootDispatcher::~PVRootDispatcher()
{
}

void PVRootDispatcher::Start()
{
    //init and start sip client
    m_SipClient.reset(new PVSipClient(VOICE_BUFFER_PORTION_SIZE, m_sharedInputBuffer.get(), m_sharedOutputBuffer.get(), this));
    m_SipClient->Connect();

    //TODO - add here system signal handling
    //create main application loop
    while(true)
    {
        WaitMessage();
    }
}
