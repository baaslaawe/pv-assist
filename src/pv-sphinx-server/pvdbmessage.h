#ifndef PVDBMESSAGE_H
#define PVDBMESSAGE_H

#include "./pvmessage.h"
#include <map>
#include <string>


class PVDBMessage : public PVMessage
{
private:
    typedef std::map<std::string, std::string> MAPSTR2STR;
    MAPSTR2STR m_aQueryParams;
    std::string m_tableName;
public:
    PVDBMessage(std::string tableName);
    void AddParam(std::string name, std::string value);
    std::string GetFields();
    std::string GetValues();
    virtual ~PVDBMessage();
};

#endif // PVDBMESSAGE_H
