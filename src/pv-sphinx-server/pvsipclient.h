#ifndef PVSIPCLIENT_H
#define PVSIPCLIENT_H

#include "../pjproject-2.0-beta/pjsip/include/pjsua-lib/pjsua.h"
#include "pvsipclientconfig.h"
#include <pthread.h>
#include <map>

#include "pvspeechrecognizer.h"


class PVRingBuffer;
class PVSipClient;
class PVMessageObserver;

typedef std::map<pjsua_acc_id, PVSipClient *> SIPCLIENTMAP;

/*Class represents entity for dealing with sip voice calls, it may write call data into ring buffer
if it provided in constructor.*/
class PVSipClient
{
public:
    enum PVSipClientError
    {
        PJSIP_CREATE_ERROR = 0,
        PJSIP_INIT_ERROR,
        MEMORY_ALLOCATION_ERROR,
        WRONG_BUFFER_SIZE,
        MEM_WRITER_PORT_ERROR,
        MEM_READER_PORT_ERROR,
        WRONG_CREDENTIALS
    };

    enum PVSipClientState
    {
        STATE_UNDEFINED = 0,
        STATE_CONNECTED,
        STATE_CONNECTION_ERROR,
        STATE_DISCONNECTED,
        STATE_MEDIA_STARTED,
        STATE_MEDIA_STOPED,
        STATE_TRYING_TO_CALL
    };
private:
    PVSipClientConfig m_config;
    PVSipClientState m_state;
    int m_iInternalBufferSize;
    PVRingBuffer * m_lpOutputExternalBuffer;
    PVRingBuffer * m_lpInputExternalBuffer;

    static SIPCLIENTMAP clientMap;
    static pthread_mutex_t clientMapMutex;

    pthread_t m_connectionThread;
    pj_thread_desc m_pjsipThreadDesc;
    pj_thread_t * m_pjsipThread;
    static pthread_cond_t connectionDestroyEvent;
    static pthread_mutex_t connectionDestroyMutex;

    PVMessageObserver * m_lpReceiver;

    /* Callback called by the library upon receiving incoming call */
    static void OnIncomingCall(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata);

    /* Callback called by the library when call's state has changed */
    static void OnCallState(pjsua_call_id call_id, pjsip_event *e);

    static void OnStreamCreated(pjsua_call_id call_id, pjmedia_stream *strm,
                             unsigned stream_idx, pjmedia_port **p_port);

    static void OnRegistrationStatus(pjsua_acc_id acc_id, pjsua_reg_info *info);

    /* Callback called by the library when call's media state has changed */
    static void OnCallMediaState(pjsua_call_id call_id);

    static pj_status_t  OnMemoryBufferFull(pjmedia_port *port, void *usr_data);
    static pj_status_t  OnPlayMemoryBufferEmpty(pjmedia_port *port, void *usr_data);

    static void * ConnectionThreadProc(void * lpSipClient);

    bool AddVoiceData(char * data, int iSize);
    bool GetVoiceData(char *data, int iSize);

    bool ConnectInternal();
    bool PlaceCall(char * destUri);
    void DestroyResources(void);
    void SendDbNotifyMessage(std::string sMsg);

public:
    PVSipClient(int iBufferSize, PVRingBuffer * lpOutputBuffer, PVRingBuffer * lpInputBuffer,
                PVMessageObserver *lpReceiver);
    PVSipClientConfig * GetConfig() {return (&m_config);}
    PVMessageObserver * GetReceiver() {return (m_lpReceiver);}
    PVSipClientState GetState() {return(m_state);}
    int GetBufferSize() {return (m_iInternalBufferSize);}
    void SetState(PVSipClientState value) {m_state = value;}

    bool Connect();
    virtual ~PVSipClient();
};

#endif // PVSIPCLIENT_H
