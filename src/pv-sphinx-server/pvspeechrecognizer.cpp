#include "pvspeechrecognizer.h"
#include "pvringbuffer.h"
#include "pvspeechrecognizedmessage.h"
#include "../pocketsphinx-0.7/src/libpocketsphinx/ngram_search.h"

//TODO - add sphinxbase to project folder remove hardcoded paths
#include "/home/kvant/src/sphinxbase-0.7/src/libsphinxbase/lm/ngram_model_internal.h"
#include "/home/kvant/src/sphinxbase-0.7/src/libsphinxbase/lm/ngram_model_arpa.h"
#include "/home/kvant/src/sphinxbase-0.7/src/libsphinxbase/lm/lm3g_model.h"

#include <ngram_model.h>
#include <unistd.h>
#include <sys/time.h>
#include "pvutils.h"

bool PVSpeechRecognizer::connectionDestroyEvent;
pthread_mutex_t PVSpeechRecognizer::connectionDestroyMutex;
FILE * PVSpeechRecognizer::m_testVoiceFile = NULL;
PVRingBuffer * PVSpeechRecognizer::lpExternalBuffer = NULL;

//#define TEST_BUFFER 0
#define MAX_CANDIDATE_AMOUNT 10

//TODO - add logging in this class
PVSpeechRecognizer::PVSpeechRecognizer(int argc, char *argv[], int iBufferSize, PVRingBuffer *lpBuffer, PVMessageObserver * lpReceiver)
{
    if (iBufferSize == 0)
        throw (WRONG_BUFFER_SIZE);

    m_iInternalBufferSize = iBufferSize;

    cmd_ln_t *config = cmd_ln_parse_r(NULL, cont_args_def, argc, argv, FALSE);
    if (config == NULL)
        throw(WRONG_CONFIGUTATION);

    //init main pocketsphinx module control structure
    m_decoder = NULL;
    m_decoder = ps_init(config);
    if (m_decoder == NULL)
        throw(SPHINX_INIT_ERROR);

    InitInternalLM(config);

    //init virtual audio device
    if ((m_audioDevice = (ad_rec_t *) calloc(1, sizeof(ad_rec_t))) == NULL)
        throw(MEMORY_ALLOCATION_ERROR);

    m_audioDevice->bps = 2;
    m_audioDevice->sps = 8000;

    m_connectionThread = -1;

    lpExternalBuffer = lpBuffer;

    m_lpReceiver = lpReceiver;
}

bool PVSpeechRecognizer::InitInternalLM(cmd_ln_t * config)
{
    if (config == NULL)
        return (false);

    //extract language model n-grams
    const char * lmPath = cmd_ln_str_r(config, "-lm");
    ngram_iter_t * it;
    int out1, out2;
    std::string word_str;

    ngram_model_t * lm = ngram_model_read(config, lmPath, NGRAM_AUTO, m_decoder->acmod->lmath);

    if (lm != NULL)
    {
//        for(int i = 0; i < lm->n_words; ++i)
//        {
//            std::string word_str = lm->word_str[i];
//            m_InternalLM.lmSearchNGrams.insert(word_str);
//            m_InternalLM.lmUnigrams.push_back(word_str);

//            ngram_iter_t * it = ngram_model_mgrams(lm, 2);
//            int out1, out2;
//            const int * wids = ngram_iter_get(it, &out1, &out2);
//            std::string word_str1 = ngram_word(lm, wids[0]);
//            word_str1 += ngram_word(lm, wids[1]);
//        }

        //extract unigrams
        it = ngram_model_mgrams(lm, 0);
        while(it != NULL)
        {
            const int * wids = ngram_iter_get(it, &out1, &out2);
            word_str = ngram_word(lm, wids[0]);

            if (m_InternalLM.lmSearchNGrams.find(word_str) == m_InternalLM.lmSearchNGrams.end())
            {
                m_InternalLM.lmUnigrams.push_back(word_str);
                m_InternalLM.lmSearchNGrams.insert(word_str);
            }

            it = ngram_iter_next(it);
        }

         //extract bigrams
        it = ngram_model_mgrams(lm, 1);
        while(it != NULL)
        {
            const int * wids = ngram_iter_get(it, &out1, &out2);
            word_str = "";
            std::string tmp = ngram_word(lm, wids[0]);
            if (tmp != "<s>")
                word_str += tmp;
            else
            {
                it = ngram_iter_next(it);
                continue;
            }

            tmp = ngram_word(lm, wids[1]);
            if (tmp != "</s>")
                word_str += std::string(" ") + tmp;
            else
            {
                it = ngram_iter_next(it);
                continue;
            }

            if (m_InternalLM.lmSearchNGrams.find(word_str) == m_InternalLM.lmSearchNGrams.end())
            {
                m_InternalLM.lmBigrams.push_back(word_str);
                m_InternalLM.lmSearchNGrams.insert(word_str);
            }

            it = ngram_iter_next(it);
        }

         //extract trigrams
        it = ngram_model_mgrams(lm, 2);
        while(it != NULL)
        {
            const int * wids = ngram_iter_get(it, &out1, &out2);
            word_str = "";
            std::string tmp = ngram_word(lm, wids[0]);
            if (tmp != "<s>")
                word_str += tmp;
            else
            {
                it = ngram_iter_next(it);
                continue;
            }

            word_str += std::string(" ") + ngram_word(lm, wids[1]);

            tmp = ngram_word(lm, wids[2]);
            if (tmp != "</s>")
                word_str += std::string(" ") + tmp;
            else
            {
                it = ngram_iter_next(it);
                continue;
            }

            if (m_InternalLM.lmSearchNGrams.find(word_str) == m_InternalLM.lmSearchNGrams.end())
            {
                m_InternalLM.lmTrigrams.push_back(word_str);
                m_InternalLM.lmSearchNGrams.insert(word_str);
            }

            it = ngram_iter_next(it);
        }

        return (true);
    }

    return (false);
}

bool PVSpeechRecognizer::Start()
{
    connectionDestroyEvent = false;

    if (pthread_mutex_init(&connectionDestroyMutex, NULL))
        return (false);

    if (pthread_create( &m_connectionThread, NULL, (void *(*)(void *))&ConnectionThreadProc, this) != 0)
        return (false);

    return (true);
}

bool PVSpeechRecognizer::StartInternal()
{
    //TODO - add error codes
#ifdef TEST_BUFFER
    if ((m_continiousListener = cont_ad_init(m_audioDevice, AudioDeviceEmulationTestRead)) == NULL)
        return (false);
#else
    if ((m_continiousListener = cont_ad_init(m_audioDevice, AudioDeviceBufferRead)) == NULL)
        return (false);
#endif

    if (ad_start_rec(m_audioDevice) < 0)
        return (false);

    if (cont_ad_calib(m_continiousListener) < 0)
        return (false);

    return (true);
}

bool PVSpeechRecognizer::RecognizeCycle(bool bEnableInternalCycle)
{
    int16 adbuf[4096];
    int32 k, ts, rem;
    char const *hyp;
    char const *uttid;
    cont_ad_t *cont;
    char word[256];

    //Wait data for next utterance
    while ((k = cont_ad_read(m_continiousListener, adbuf, m_iInternalBufferSize)) == 0)
    {
        if (QueryDestroyEvent())
            return (false);
        usleep(100);
    }

    if (k < 0)
        return (false);

    /*Non-zero amount of data received; start recognition of new utterance.
        NULL argument to uttproc_begin_utt => automatic generation of utterance-id.*/
    if (ps_start_utt(m_decoder, NULL) < 0)
        return (false);

    ps_process_raw(m_decoder, adbuf, k, FALSE, FALSE);

    //Note timestamp for this first block of data
    ts = m_continiousListener->read_ts;

    while (!QueryDestroyEvent())
    {
        /* Read non-silence audio data, if any, from continuous listening module */
        if ((k = cont_ad_read(m_continiousListener, adbuf, m_iInternalBufferSize)) < 0)
            return (false);
        if (k == 0)
        {
            /*No speech data available; check current timestamp with most recent
             speech to see if more than 1 sec elapsed.  If so, end of utterance */
            if ((m_continiousListener->read_ts - ts) > DEFAULT_SAMPLES_PER_SEC)
                break;
        }
        else
        {
            //New speech data received; note current timestamp
            ts = m_continiousListener->read_ts;
        }

        //Decode whatever data was read above
        rem = ps_process_raw(m_decoder, adbuf, k, FALSE, FALSE);

        //If no work to be done, sleep a bit
        if ((rem == 0) && (k == 0))
            usleep(80);
    }

#ifdef TEST_BUFFER
    AudioDeviceEmulationTestRead(m_audioDevice, adbuf, m_iInternalBufferSize);
#else
    AudioDeviceBufferRead(m_audioDevice, adbuf, m_iInternalBufferSize);
#endif

    cont_ad_reset(m_continiousListener);

    //bet best hypothesis
    ps_end_utt(m_decoder);
    hyp = ps_get_hyp(m_decoder, NULL, &uttid);

    if (strlen(hyp) != 0 && m_lpReceiver != NULL)
    {
        //search MAX_CANDIDATE_AMOUNT candidates in word graph
        ps_nbest_ext_t * ext;
        int extListLength = MAX_CANDIDATE_AMOUNT;
        ps_get_nbest_hyp_ext(m_decoder, &ext, &extListLength);
        int32 ix, jx;
        for (ix = 0; ix < extListLength; ix++)
        {
            ps_nbest_ext_t * ext_ix = &ext[ix];
            printf("index = %d len = %d\n", ix, ext_ix->len);
            for (jx = 0; jx < ext_ix->len; jx++)
            {
                printf("wordids = %d score = %d word = %s total = %d\n\n",
                       ext_ix->wordids[jx], ext_ix->scores[jx], ext_ix->words[jx], ext_ix->total_score);
                fflush(stdout);
            }
        }

        //create message with recognition results
        std::tr1::shared_ptr<PVSpeechRecognizedMessage> msg(new PVSpeechRecognizedMessage(hyp));
        msg->ImportFromPocketSphinx(ext, extListLength);

        //destroy resources
        ps_nbest_ext_free(&ext, extListLength);

        m_lpReceiver->SendMessage(msg);
    }

    return (true);
}

PVSpeechRecognizer::~PVSpeechRecognizer()
{
    //TODO - use timelimited join
    pthread_mutex_lock(&connectionDestroyMutex);
    connectionDestroyEvent = true;
    pthread_mutex_unlock(&connectionDestroyMutex);

    if (m_connectionThread != -1)
        pthread_join(m_connectionThread, NULL);

    //then kill sphinx library
    cont_ad_close(m_continiousListener);
    ad_close(m_audioDevice);
    ps_free(m_decoder);

    //destroy resources
    pthread_mutex_destroy(&connectionDestroyMutex);
    lpExternalBuffer = NULL;
}

void *PVSpeechRecognizer::ConnectionThreadProc(void *lpRecognizer)
{
    PVSpeechRecognizer * recognizer = (PVSpeechRecognizer *)lpRecognizer;
    if (recognizer == NULL)
        return (NULL);

    if (!recognizer->StartInternal())
        return (NULL);

    //TODO -check return values
    while(true)
    {
        if (QueryDestroyEvent())
            break;

        recognizer->RecognizeCycle(true);

        if (QueryDestroyEvent())
            break;
    }
    return (NULL);
}

bool PVSpeechRecognizer::QueryDestroyEvent()
{
    bool bRet = false;

    pthread_mutex_lock(&connectionDestroyMutex);

    if (connectionDestroyEvent)
        bRet = true;

    pthread_mutex_unlock(&connectionDestroyMutex);

    return(bRet);
}

int32 PVSpeechRecognizer::AudioDeviceEmulationTestRead(ad_rec_t * handle, int16 * buf, int32 max)
{
    int32 length;

    length = max * handle->bps; /* #samples -> #bytes */

    if (m_testVoiceFile == NULL)
        m_testVoiceFile = fopen("../test_data/send_message3.wav", "r+");

    if ((length = fread(buf, 1, length, m_testVoiceFile)) > 0)
    {
        length /= handle->bps;
    }

    if (length < 0)
    {
        if (errno != EAGAIN)
            return AD_ERR_GEN;
        else
            length = 0;
    }

    if ((length == 0) && (!handle->recording))
        return AD_EOF;

    return length;
}

int32 PVSpeechRecognizer:: AudioDeviceBufferRead(ad_rec_t * handle, int16 * buf, int32 max)
{
    int32 length;

    length = max * handle->bps; /* #samples -> #bytes */

    if (lpExternalBuffer == NULL)
        return (-1);

    if ((length = lpExternalBuffer->ReadData((char*)buf, length)) > 0)
    {
        length /= handle->bps;
    }

    if (length < 0)
    {
        if (errno != EAGAIN)
            return AD_ERR_GEN;
        else
            length = 0;
    }

    if ((length == 0) && (!handle->recording))
        return AD_EOF;

    return length;
}

void PVSpeechRecognizer::OnMessage(std::tr1::shared_ptr<PVMessage> inMsg)
{
    //switch(inMsg->GetType())
    //{

    //}
}
