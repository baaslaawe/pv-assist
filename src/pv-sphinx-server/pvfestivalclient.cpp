#include "pvfestivalclient.h"
#include "pvappconfig.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <vector>
#include "rateconv.h"

extern PVAppConfig g_appConfig;

#define SWAPSHORT(x) ((((unsigned)x) & 0xff) << 8 | \
                      (((unsigned)x) & 0xff00) >> 8)
#define SWAPINT(x) ((((unsigned)x) & 0xff) << 24 | \
                    (((unsigned)x) & 0xff00) << 8 | \
            (((unsigned)x) & 0xff0000) >> 8 | \
                    (((unsigned)x) & 0xff000000) >> 24)

/* Sun, HP, SGI Mips, M68000 */
//#define FAPI_BIG_ENDIAN
/* Intel, Alpha, DEC Mips, Vax */
#define FAPI_LITTLE_ENDIAN
#define START_BUFFER_SIZE 1024
#define WAV_RAW_DATA_START 44
#define FESTIVAL_SAMPLE_RATE 16000
#define PVASSIST_SAMPLE_RATE 8000

PVFestivalClient::PVFestivalClient()
    :m_Info(NULL)
{
}

void PVFestivalClient::DeleteFTInfo(FT_Info *info)
{
    if (info != NULL)
        free(info);
}

void PVFestivalClient::SocketReceive2Buffer(int fd, std::vector<char> & outputBuffer)
{
    /* Receive file (probably a waveform file) from socket using   */
    /* Festival key stuff technique, but long winded I know, sorry */
    /* but will receive any file without closeing the stream or    */
    /* using OOB data                                              */
    static char * file_stuff_key = "ft_StUfF_key"; /* must == Festival's key */
    int bufflen;
    int n, k, i;
    char c;

    //TODO - remove raw memory allocation
    bufflen = START_BUFFER_SIZE;
    outputBuffer.resize(bufflen);
    int size = 0;

    for (k = 0; file_stuff_key[k] != '\0';)
    {
        n = read(fd, &c ,1);
        if (n == 0)
            break;  /* hit stream eof before end of file */

        if (size + k + 1 >= bufflen)
        {
            /* +1 so you can add a NULL if you want */
            bufflen += bufflen/4;
            outputBuffer.resize(bufflen);
        }

        if (file_stuff_key[k] == c)
        {
            k++;
        }
        else if ((c == 'X') && (file_stuff_key[k+1] == '\0'))
        {
            /* It looked like the key but wasn't */
            for ( i=0; i < k; i++, size++)
                outputBuffer[size] = file_stuff_key[i];
            k = 0;
            /* omit the stuffed 'X' */
        }
        else
        {
            for (i=0; i < k; i++,(size)++)
                outputBuffer[size] = file_stuff_key[i];

            k = 0;
            outputBuffer[size] = c;
            size++;
        }

    }
}

PVFestivalClient::~PVFestivalClient()
{
    Close();
}

FT_Info * PVFestivalClient::CreateDefaultInfo()
{
    FT_Info *info;
    info = (FT_Info *)malloc(1 * sizeof(FT_Info));

    info->server_host = FESTIVAL_DEFAULT_SERVER_HOST;
    info->server_port = FESTIVAL_DEFAULT_SERVER_PORT;
    info->text_mode = FESTIVAL_DEFAULT_TEXT_MODE;

    info->server_fd = -1;

    return info;
}

bool PVFestivalClient::Open(FT_Info *info)
{
    /* Open socket to server */

    if (info == NULL)
        m_Info = CreateDefaultInfo();
    else
        m_Info = info;

    if (m_Info == NULL)
        return (false);

    m_Info->server_fd = FestivalSocketOpen(m_Info->server_host, m_Info->server_port);

    if (m_Info->server_fd == -1)
        return false;

    return true;
}

int PVFestivalClient::FestivalSocketOpen(const char *host, int port)
{
    /* Return an FD to a remote server */
    struct sockaddr_in serv_addr;
    struct hostent *serverhost;
    int fd;

    fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (fd < 0)
    {
        fprintf(stderr,"festival_client: can't get socket\n");
        return -1;
    }

    memset(&serv_addr, 0, sizeof(serv_addr));
    if ((serv_addr.sin_addr.s_addr = inet_addr(host)) == -1)
    {
        /* its a name rather than an ipnum */
        serverhost = gethostbyname(host);
        if (serverhost == (struct hostent *)0)
        {
            fprintf(stderr,"festival_client: gethostbyname failed\n");
            return -1;
        }
        memmove(&serv_addr.sin_addr,serverhost->h_addr, serverhost->h_length);
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    if (connect(fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) != 0)
    {
        fprintf(stderr,"festival_client: connect to server failed\n");
        return -1;
    }

    return fd;
}

bool PVFestivalClient::StringToWave(std::string sText, std::vector<char> & outputBuffer)
{
    FILE * festivalDevice;
    char strIterator;
    char ack[4];
    int n;
    std::vector<char> internalBuffer;

    if (m_Info == 0)
        return false;

    if (m_Info->server_fd == -1)
    {
        fprintf(stderr,"festival_client: server connection unopened\n");
        return false;
    }

    festivalDevice = fdopen(dup(m_Info->server_fd), "wb");

    /* Copy text over to server, escaping any quotes */
    fprintf(festivalDevice,"(tts_textall \"\n");

    for (int i = 0; i < sText.size(); ++i)
    {
        strIterator = sText[i];
        if (strIterator == '\0')
            break;

        if ((strIterator == '"') || (strIterator == '\\'))
            putc('\\', festivalDevice);

        putc(strIterator, festivalDevice);
    }

    fprintf(festivalDevice,"\" \"%s\")\n", m_Info->text_mode);
    fclose(festivalDevice);

    //TODO - add code for dropping buffer in case of unxepected return with help of auto memory managment

    /* Read back info from server */
    /* This assumes only one waveform will come back, also LP is unlikely */
    do
    {
        for (n = 0; n < 3; )
            n += read(m_Info->server_fd, ack + n, 3 - n);
        ack[3] = '\0';

        /* receive an s-expr - which meanans raw sound samples*/
        if (strcmp(ack,"WV\n") == 0 || strcmp(ack,"LP\n") == 0)
            ClientAcceptExpr(m_Info->server_fd, internalBuffer);
        else
        {
            /* server got an error */
            fprintf(stderr,"festival_client: all data had been read - try to process it\n");

            //add postprocessing - removing WAV header and downsample sound data
            //internalBuffer.erase(internalBuffer.begin(), internalBuffer.begin() + WAV_RAW_DATA_START);

            //TODO - refactor this old c-style
            //short * newdata;
            //int osize;
            //rateconv((short *)&internalBuffer[0], internalBuffer.size(), &newdata, &osize,
            //         FESTIVAL_SAMPLE_RATE, PVASSIST_SAMPLE_RATE);
            //outputBuffer.assign(newdata, newdata + osize);
            //delete [] newdata;
            outputBuffer.assign(internalBuffer.begin(), internalBuffer.end());

            return true;
        }
    }
    while (strcmp(ack,"OK\n") != 0);

    return true;
}

void PVFestivalClient::ClientAcceptExpr(int fd, std::vector<char> & buffer)
{
    std::vector<char> dataChunk;
    SocketReceive2Buffer(fd, dataChunk);
    buffer.insert(buffer.end(), dataChunk.begin(), dataChunk.end());
}

bool PVFestivalClient::Close()
{
    if (m_Info == 0)
        return false;

    if (m_Info->server_fd != -1)
        close(m_Info->server_fd);

    DeleteFTInfo(m_Info);

    return true;
}


