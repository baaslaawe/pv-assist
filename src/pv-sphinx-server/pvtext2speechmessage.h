#ifndef PVTEXT2SPEECHMESSAGE_H
#define PVTEXT2SPEECHMESSAGE_H

#include "pvmessage.h"
#include <string>

class PVText2SpeechMessage : public PVMessage
{
private:
    std::string m_sRecognitionResult;
public:
    PVText2SpeechMessage(std::string sText);
    std::string GetText() {return (m_sRecognitionResult);}
};

#endif // PVTEXT2SPEECHMESSAGE_H
