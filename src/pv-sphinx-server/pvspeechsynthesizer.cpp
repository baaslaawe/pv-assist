#include "pvspeechsynthesizer.h"
#include "pvfestivalclient.h"
#include "pvtext2speechmessage.h"
#include "pvringbuffer.h"
#include <stdio.h>

#define FESTIVAL_SAMPLE_RATE 16000
#define PVASSIST_SAMPLE_RATE 8000

//TODO - extrude those definitions to separate class
bool PVSpeechSynthesizer::connectionDestroyEvent;
pthread_mutex_t PVSpeechSynthesizer::connectionDestroyMutex;
//PVRateConverter PVSpeechSynthesizer::rateConverter(FESTIVAL_SAMPLE_RATE, PVASSIST_SAMPLE_RATE);

#define USE_VOICE_DUMP_FILE
#define VOICE_DUMP_FILE "../test_data/synth_voice.wav"
#define WAV_RAW_DATA_START 44

PVSpeechSynthesizer::PVSpeechSynthesizer(PVRingBuffer *lpBuffer)
    :m_synthClient(new PVFestivalClient())
{
    m_lpExternalBuffer = lpBuffer;

    m_connectionThread = -1;

#ifdef USE_VOICE_DUMP_FILE
    m_voiceDumpFile = fopen(VOICE_DUMP_FILE, "w");
#endif
}

PVSpeechSynthesizer::~PVSpeechSynthesizer()
{
    //TODO - use timelimited join
    pthread_mutex_lock(&connectionDestroyMutex);
    m_MsgQ.set_destroy_condition();
    pthread_mutex_unlock(&connectionDestroyMutex);

    if (m_connectionThread != -1)
        pthread_join(m_connectionThread, NULL);

#ifdef USE_VOICE_DUMP_FILE
    fclose(m_voiceDumpFile);
#endif
}

bool PVSpeechSynthesizer::Start()
{
    connectionDestroyEvent = false;

    if (pthread_mutex_init(&connectionDestroyMutex, NULL))
        return (false);

    if (pthread_create( &m_connectionThread, NULL, (void *(*)(void *))&ConnectionThreadProc, this) != 0)
        return (false);

    return (true);
}

void PVSpeechSynthesizer::OnMessage(std::tr1::shared_ptr<PVMessage> inMsg)
{
    switch(inMsg->GetType())
    {
    case PVMessage::TEXT_TO_SPEECH_MSG:
        {
            PVText2SpeechMessage * lpMsg = dynamic_cast<PVText2SpeechMessage *>(inMsg.get());
            if (lpMsg != NULL)
            {
                std::vector<char> outputBuffer, inputBuffer;
                m_synthClient->StringToWave(lpMsg->GetText(), outputBuffer);

                //remove wav header from buffer
                outputBuffer.erase(outputBuffer.begin(), outputBuffer.begin() + WAV_RAW_DATA_START);
                PVRateConverter rateConverter(16000, 8000);
                if (rateConverter.Resample(outputBuffer, inputBuffer))
                {
                    m_lpExternalBuffer->WriteData(&inputBuffer[0], inputBuffer.size());
#ifdef USE_VOICE_DUMP_FILE
                    fwrite(&inputBuffer[0], 2, inputBuffer.size()/2, m_voiceDumpFile);
#endif
                }
            }
            break;
        }
    default:
        {
        }
    }
}

void *PVSpeechSynthesizer::ConnectionThreadProc(void *lpSynthesizer)
{
    PVSpeechSynthesizer * synthesizer = (PVSpeechSynthesizer *)lpSynthesizer;
    if (synthesizer == NULL)
        return (NULL);

    if (!synthesizer->StartInternal())
        return (NULL);

    //main thread loop
    while(true)
    {
        if (!synthesizer->WaitMessageSafe())
            return (NULL);
    }

    return (NULL);
}

bool PVSpeechSynthesizer::StartInternal()
{
    if(!m_synthClient->Open())
        return (false);

    return (true);
}
