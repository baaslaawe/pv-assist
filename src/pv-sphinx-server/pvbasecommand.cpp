#include "pvbasecommand.h"
#include "pvtext2speechmessage.h"
#include "pvmessageobserver.h"
#include "pvtext2speechmessage.h"
#include "pvgetweathercommand.h"
#include "./pvdbsavecommand.h"
#include <tr1/memory>
#include <algorithm>


#define DEFAULT_COMMAND_TEXT "Your command is"
#define WEATHER_COMAND_TEXT "weather"

PVBaseCommand * PVBaseCommand::CreateCommand(PVMessage *lpMsg, PVMessageObserver *lpReceiver)
{
    if (lpMsg == NULL)
        return (NULL);

    if (lpMsg->GetType() == PVMessage::TEXT_TO_SPEECH_MSG)
    {
        PVText2SpeechMessage * lpTextMsg = dynamic_cast<PVText2SpeechMessage *>(lpMsg);

        if (lpTextMsg == NULL)
            return (NULL);

        std::string msgText = lpTextMsg->GetText();
        std::transform(msgText.begin(), msgText.end(), msgText.begin(), ::tolower);

        if (msgText == WEATHER_COMAND_TEXT)
        {
            return (new PVGetWeatherCommand(lpReceiver));
        }
        else
        {
            return (new PVBaseCommand(lpReceiver, lpTextMsg->GetText()));
        }

    }
    else if (lpMsg->GetType() == PVMessage::DB_TASK_MSG)
    {
        return (new PVDbSaveCommand(lpReceiver, lpMsg));
    }
    else
        return (NULL);
}

PVBaseCommand::PVBaseCommand(PVMessageObserver *lpReceiver, std::string sText)
    :m_lpReceiver(lpReceiver),
      m_sText(sText)
{
}

PVBaseCommand::~PVBaseCommand()
{
}

bool PVBaseCommand::Execute()
{
    std::tr1::shared_ptr<PVMessage> msg(
                new PVText2SpeechMessage(std::string(DEFAULT_COMMAND_TEXT) + " " + m_sText));
    m_lpReceiver->SendMessage(msg);

    return (true);
}
