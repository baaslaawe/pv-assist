#ifndef PVHEURISTICANALYZER_H
#define PVHEURISTICANALYZER_H

#include "pvmessageobserver.h"
#include <vector>
#include <tr1/memory>
#include <list>

class PVBaseHeuristic;
class PVSpeechRecognizedMessage;
struct InternalLM;

class PVHeuristicAnalyzer : public PVMessageObserver
{
public:
   enum PVHeuristicAnalyzerError
   {
       NULL_RECEIVER_POINTER_ERROR,
       LM_INIT_ERROR
   };
private:
    typedef std::list<std::tr1::shared_ptr<PVBaseHeuristic> > HEURISTICLIST;
    HEURISTICLIST m_CommandMap;
    void ClearResults();
    void OnRecognitionResultMessage(PVSpeechRecognizedMessage *lpMsg);
    PVMessageObserver * m_lpReceiver;
    std::auto_ptr<InternalLM> m_InternalRecognizerLM;
public:
    PVHeuristicAnalyzer(PVMessageObserver * lpReceiver, InternalLM * lm);
    virtual void OnMessage(std::tr1::shared_ptr<PVMessage> inMsg);
};

#endif // PVHEURISTICANALYZER_H
