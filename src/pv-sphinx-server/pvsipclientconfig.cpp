#include "pvsipclientconfig.h"

PVSipClientConfig::PVSipClientConfig()
{
    char tmp[80];
    unsigned i;

    pjsua_config_default(&cfg);
    pj_ansi_sprintf(tmp, "PJSUA v%s %s", pj_get_version(),
            pj_get_sys_info()->info.ptr);
    //pj_strdup2_with_null(my_config.pool, &cfg.user_agent, tmp);

    pjsua_logging_config_default(&log_cfg);
    pjsua_media_config_default(&media_cfg);
    pjsua_transport_config_default(&udp_cfg);
    udp_cfg.port = 5060;
    pjsua_transport_config_default(&rtp_cfg);
    rtp_cfg.port = 4000;
    redir_op = PJSIP_REDIRECT_ACCEPT;

    pjsua_acc_config_default(&acc_cfg);
}

bool PVSipClientConfig::SerializeConfig(std::string fileName)
{
    return (false);
}

bool PVSipClientConfig::DeserializeConfig(std::string fileName)
{
    return (false);
}
