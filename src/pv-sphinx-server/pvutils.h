#ifndef PVUTILS_H
#define PVUTILS_H

#include <string>
#include <vector>
#include <iostream>
#include <sstream>

namespace pvutils
{
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
    std::vector<std::string> split(const std::string &s, char delim);
    std::string &trim(std::string &s);
    std::string &ltrim(std::string &s);
    std::string &rtrim(std::string &s);
    bool isdigit(std::string &s);
}
#endif // PVUTILS_H
