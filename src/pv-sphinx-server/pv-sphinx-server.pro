TEMPLATE = app
CONFIG += console
CONFIG -= qt

#QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp \
    pvsipclient.cpp \
    pvsipclientconfig.cpp \
    pvringbuffer.cpp \
    ringbuffer.c \
    pvspeechrecognizer.cpp \
    pvscopedlock.cpp \
    pvmessage.cpp \
    pvmessageobserver.cpp \
    pvrootdispatcher.cpp \
    pvsipstatemessage.cpp \
    pvspeechsynthesizer.cpp \
    pvfestivalclient.cpp \
    pvtext2speechmessage.cpp \
    rateconv.cc \
    pvbasecommand.cpp \
    pvgetweathercommand.cpp \
    pvspeechrecognizedmessage.cpp \
    pvheuristicanalyzer.cpp \
    pvbaseheuristic.cpp \
    pvutils.cpp \
    pvrateconverter.cpp \
    pvappconfig.cpp \
    tests/pvrateconvertertest.cpp \
    tests/pvsipclienttest.cpp \
    tests/pvspeechrecognizertest.cpp \
    pvdbmessage.cpp \
    pvdbsavecommand.cpp

HEADERS += \
    pvsipclient.h \
    pvsipclientconfig.h \
    pvringbuffer.h \
    ringbuffer.h \
    pvspeechrecognizer.h \
    pvconcurentqueue.h \
    pvscopedlock.h \
    pvmessage.h \
    pvmessageobserver.h \
    pvrootdispatcher.h \
    pvsipstatemessage.h \
    pvspeechsynthesizer.h \
    pvfestivalclient.h \
    pvtext2speechmessage.h \
    rateconv.h \
    pvbasecommand.h \
    pvgetweathercommand.h \
    pvutils.h \
    pvspeechrecognizedmessage.h \
    pvheuristicanalyzer.h \
    pvbaseheuristic.h \
    pvrateconverter.h \
    pvappconfig.h \
    tests/pvrateconvertertest.h \
    tests/pvsipclienttest.h \
    tests/pvspeechrecognizertest.h \
    pvdbmessage.h \
    pvdbsavecommand.h

#TODO - remove hardcoded paths from project

INCLUDEPATH += ../pjproject-2.0-beta/pjlib/include \
               ../pjproject-2.0-beta/pjlib-util/include \
                ../pjproject-2.0-beta/pjnath/include \
                ../pjproject-2.0-beta/pjmedia/include \
                ../pjproject-2.0-beta/pjsip/include \
                /usr/local/include/sphinxbase \
                ../pocketsphinx-0.7/include \
                ../pocketsphinx-0.7/src/libpocketsphinx \
                /usr/local/include/curl \
                /usr/local/include/libavcodec \
                /usr/local/include/libavformat \
                /usr/local/include/cppunit \
#                /home/kvant/src/festival/src/include \
#                /home/kvant/src/speech_tools/include

LIBS += -L../pjproject-2.0-beta/pjlib/lib\
        -L../pjproject-2.0-beta/pjlib-util/lib\
        -L../pjproject-2.0-beta/pjnath/lib\
        -L../pjproject-2.0-beta/pjmedia/lib\
        -L../pjproject-2.0-beta/pjsip/lib\
        -L../pjproject-2.0-beta/third_party/lib\
        -lpjsua-i686-pc-linux-gnu\
        -lpjsip-ua-i686-pc-linux-gnu\
        -lpjsip-simple-i686-pc-linux-gnu\
        -lpjsip-i686-pc-linux-gnu\
        -lpjmedia-codec-i686-pc-linux-gnu\
        -lpjmedia-videodev-i686-pc-linux-gnu\
        -lpjmedia-i686-pc-linux-gnu\
        -lpjmedia-audiodev-i686-pc-linux-gnu\
        -lpjnath-i686-pc-linux-gnu\
        -lpjlib-util-i686-pc-linux-gnu\
        -lresample-i686-pc-linux-gnu\
        -lmilenage-i686-pc-linux-gnu\
        -lsrtp-i686-pc-linux-gnu\
        -lgsmcodec-i686-pc-linux-gnu\
        -lspeex-i686-pc-linux-gnu\
        -lilbccodec-i686-pc-linux-gnu\
        -lg7221codec-i686-pc-linux-gnu\
        -lportaudio-i686-pc-linux-gnu\
        -lpj-i686-pc-linux-gnu\
        -lz\
        -lm\
        -lnsl\
        -lrt\
        -lpthread\
        -lcrypto\
        -lssl\
        -L/usr/local/lib \
        -lsphinxbase \
        -lsphinxad \
        -lpocketsphinx \
        -lcurl \
        -lavcodec \
        -lavutil \
        -lz \
        -lm \
        -lcppunit \
        -ldl \
        -L/usr/local/lib/mysql \
        -lmysqlclient \
#        -L/home/kvant/src/speech_tools/lib \
#        -lestools \
#        -lestbase \
#        -L/home/kvant/src/festival/src/lib \
#        -lFestival
