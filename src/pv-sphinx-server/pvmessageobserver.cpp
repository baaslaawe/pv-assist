#include "pvmessageobserver.h"
#include "pvmessage.h"

void PVMessageObserver::SendMessage(std::tr1::shared_ptr<PVMessage> inMsg)
{
    m_MsgQ.push(inMsg);
}

void PVMessageObserver::SendMessageSync(std::tr1::shared_ptr<PVMessage> inMsg)
{
    OnMessage(inMsg);
}

void PVMessageObserver::WaitMessage()
{
    std::tr1::shared_ptr<PVMessage> msg = m_MsgQ.wait_and_pop();
    OnMessage(msg);
}

void PVMessageObserver::WaitMessage(int iWaitTimeout)
{
    std::tr1::shared_ptr<PVMessage> msg = m_MsgQ.wait_and_pop(iWaitTimeout);
    OnMessage(msg);
}

bool PVMessageObserver::WaitMessageSafe()
{
    std::tr1::shared_ptr<PVMessage> msg;
    if (m_MsgQ.wait_and_pop_safe(msg))
    {
        if (msg.get() != NULL)
            OnMessage(msg);
        return (true);
    }
    else
    {
        OnDestroy();
        return (false);
    }
}

void PVMessageObserver::CheckMessage()
{
    std::tr1::shared_ptr<PVMessage> msg = m_MsgQ.try_pop();
    if (msg.get() != NULL)
        OnMessage(msg);
}

void PVMessageObserver::OnDestroy()
{
}

PVMessageObserver::PVMessageObserver()
{
}

PVMessageObserver::~PVMessageObserver()
{
}
